import styled from "styled-components";

export const Container = styled.button`
  background: ${(props) =>
    props.whiteSchema ? "#f5f5f5" : props.logout ? "#040404" : "#04103C"};
  color: ${(props) => (props.whiteSchema ? "#04103C" : "#f5f5f5")};
  height: 45px;
  border-radius: 8px;
  border: ${(props) =>
    props.whiteSchema ? "2px solid #0c0d0d" : "2px solid #f5f5f5"};
  font-family: "Roboto Mono", monospace;
  margin-top: 16px;
  width: ${(props) => (props.logout ? "20%" : "100%")};
  transition: 0.5s;
  :hover {
    border: 2px solid var(--blueKenzieHard);
  }
`;
