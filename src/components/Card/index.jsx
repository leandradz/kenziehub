import { Container } from "./styles";
import { FiAward, FiTrendingUp } from "react-icons/fi";
import Button from "../Button";

const Card = ({ title, status, onClick }) => {
  return (
    <Container>
      <span>
        <FiAward /> {title}
      </span>
      <hr />
      <time>
        <FiTrendingUp /> {status}
      </time>
      <Button onClick={onClick}>Apagar</Button>
    </Container>
  );
};

export default Card;
