import { Container, InputContainer } from "./styles";

const Input = ({ label, icon: Icon, register, name, error = "", ...res }) => {
  return (
    <Container>
      <div>
        {label} {!!error && <span> - {error}</span>}
      </div>

      <InputContainer isErrored={!!error}>
        {Icon && <Icon size={20} />}
        <input {...register(name)} {...res} />
      </InputContainer>
    </Container>
  );
};

export default Input;
