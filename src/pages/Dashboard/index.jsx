import { Container, InputContainer, TasksContainer } from "./styles";
import Input from "../../components/Input";
import Button from "../../components/Button";
import Card from "../../components/Card";
import { Redirect } from "react-router-dom";
import { useForm } from "react-hook-form";
import { FiBookmark } from "react-icons/fi";
import { useState } from "react";
import api from "../../service/api";
import { toast } from "react-toastify";

const Dashboard = ({ authenticated, setAuthenticated }) => {
  const { register, handleSubmit } = useForm();
  const [token] = useState(
    JSON.parse(localStorage.getItem("@Kenziehub:token")) || ""
  );
  const [user, setUser] = useState(
    JSON.parse(localStorage.getItem("@Kenziehub:user")) || ""
  );

  const onSubmit = (data) => {
    if (data.title === "" || data.status === "") {
      return toast.error("Complete o campo para enviar uma tarefa");
    }
    api
      .post("/users/techs", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        api
          .get(`/users/${user.id}`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((response) => setUser(response.data))
          .catch((err) => console.log("Erro ao Adicionar"));
      });
  };

  const handleDelete = (id) => {
    api
      .delete(`/users/techs/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => {
        api
          .get(`/users/${user.id}`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((response) => setUser(response.data))
          .catch((err) => console.log("Erro ao Deletar"));
      });
  };

  if (!authenticated) {
    return <Redirect to="/sessions" />;
  }

  return (
    <Container>
      <InputContainer onSubmit={handleSubmit(onSubmit)}>
        <h1>Cadastro de Tecnologias</h1>
        <Button logout onClick={() => setAuthenticated(!authenticated)}>
          Logout
        </Button>
        <section>
          <Input
            icon={FiBookmark}
            placeholder="Linguagem"
            register={register}
            name="title"
          />

          <select {...register("status")}>
            <option value="iniciante">Iniciante</option>
            <option value="intermediario">Intermediario</option>
            <option value="avançado">Avançado</option>
          </select>

          <Button type="submit">Adicionar</Button>
        </section>
      </InputContainer>
      <TasksContainer>
        {user.techs.map((techs) => (
          <Card
            key={techs.id}
            title={techs.title}
            status={techs.status}
            onClick={() => handleDelete(techs.id)}
          />
        ))}
      </TasksContainer>
    </Container>
  );
};

export default Dashboard;
