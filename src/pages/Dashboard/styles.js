import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 0 38px;
`;

export const InputContainer = styled.form`
  flex: 1;
  margin-top: 60px;
  padding: 0 38px;

  h1 {
    display: inline-block;
    margin-right: 40px;
  }
  section {
    display: flex;
    margin-top: 40px;

    > div {
      max-width: 350px;
      flex: 1;
      margin-right: 16px;
    }
    select {
      max-width: 250px;
      border-radius: 10px;
      height: 45px;
      flex: 1;
      margin-right: 16px;
      background-color: var(--white);
      color: var(--gray);
      border: 2px solid var(--gray);
    }
    button {
      max-width: 250px;
      height: 50px;
      margin: 0;
    }
  }
`;

export const TasksContainer = styled.div`
  padding: 0 38px;
  margin-top: 32px;
  display: flex;
  flex-wrap: wrap;

  div {
    margin-top: 16px;
    margin-right: 32px;
  }
`;
