import { Container, Content } from "./styles";
import Button from "../../components/Button";
import { Redirect, useHistory } from "react-router";

const Home = ({ authenticated }) => {
  const history = useHistory();

  const handleNavigation = (path) => {
    return history.push(path);
  };

  if (authenticated) {
    return <Redirect to="/users/techs" />;
  }

  return (
    <Container>
      <Content>
        <h1>KenzieHub</h1>
        <span>Um hub de portfólios de programadores</span>
        <div>
          <Button onClick={() => handleNavigation("/users")} whiteSchema>
            Cadastre-se
          </Button>
          <Button onClick={() => handleNavigation("/sessions")}>Login</Button>
        </div>
      </Content>
    </Container>
  );
};

export default Home;
