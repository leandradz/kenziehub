import { Container, Content, Background, AnimationContainer } from "./styles";
import Button from "../../components/Button";
import { Link, Redirect } from "react-router-dom";
import Input from "../../components/Input";
import { useForm } from "react-hook-form";
import { FiMail, FiLock } from "react-icons/fi";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../service/api";
import { toast } from "react-toastify";
import "./styles";

const Login = ({ authenticated, setAuthenticated }) => {
  const schema = yup.object().shape({
    email: yup.string().email("Email inválido").required("Campo obrigatório"),
    password: yup
      .string()
      .min(8, "Mínimo de 8 dígitos")
      .required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmitFunction = (data) => {
    api
      .post("/sessions", data)
      .then((response) => {
        const { token, user } = response.data;
        localStorage.clear();
        localStorage.setItem(
          "@Kenziehub:token",
          JSON.stringify(response.data.token)
        );
        localStorage.setItem(
          "@Kenziehub:user",
          JSON.stringify(response.data.user)
        );
        reset();
        setAuthenticated(true);
      })
      .catch((err) => toast.error("Email ou senha inválidos."));
  };

  if (authenticated) {
    //melhor que usar o history.push, nesse caso comp do react-router-dom
    return <Redirect to="/users/techs" />;
  }

  return (
    <Container>
      <Content>
        <AnimationContainer>
          <form onSubmit={handleSubmit(onSubmitFunction)}>
            <h1>Login</h1>
            <Input
              required
              icon={FiMail}
              label="Email"
              placeholder="Seu melhor e-mail"
              register={register}
              name="email"
              error={errors.email?.message}
            />
            <Input
              icon={FiLock}
              label="Senha"
              placeholder="Uma senha bem segura"
              type="password"
              register={register}
              name="password"
              error={errors.password?.message}
            />
            <Button type="submit">Enviar</Button>
            <p>
              Não tem uma conta? Faça seu <Link to="/users">cadastro</Link>.
            </p>
          </form>
        </AnimationContainer>
      </Content>
      <Background />
    </Container>
  );
};
export default Login;
