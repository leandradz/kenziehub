import { Container, Content, Background, AnimationContainer } from "./styles";
import Button from "../../components/Button";
import { Link, Redirect, useHistory } from "react-router-dom";
import Input from "../../components/Input";
import { FiUser, FiMail, FiLock } from "react-icons/fi";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../service/api";
import { toast } from "react-toastify";

const Singup = ({ authenticated }) => {
  const history = useHistory();

  const schema = yup.object().shape({
    email: yup.string().email("Email inválido").required("Campo obrigatório"),
    name: yup.string().required("Campo obrigatório"),
    bio: yup.string().required("Campo obrigatório"),
    contact: yup
      .string()
      .matches(
        /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
        "Número inválido."
      ),
    password: yup
      .string()
      .min(8, "Mínimo de 8 dígitos")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Senha deve conter ao menos uma letra maiúscula, uma minúscula, um número e um caracter especial!"
      )
      .required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmitFunction = (data) => {
    api
      .post("/users", data)
      .then((response) => {
        reset();
        toast.success("Sucesso ao criar a conta");
        history.push("/");
      })
      .catch((error) =>
        toast.error("Erro ao criar a conta, tente um e-mail diferente")
      );
  };

  if (authenticated) {
    return <Redirect to="/users/techs" />;
  }

  return (
    <Container>
      <Background />
      <Content>
        <AnimationContainer>
          <form onSubmit={handleSubmit(onSubmitFunction)}>
            <h1>Cadastro</h1>
            <Input
              icon={FiMail}
              required
              label="Email"
              placeholder="Seu melhor e-mail"
              register={register}
              name="email"
              error={errors.email?.message}
            />
            <Input
              icon={FiUser}
              label="Nome"
              placeholder="Seu nome completo"
              register={register}
              name="name"
              error={errors.name?.message}
            />
            <Input
              icon={FiLock}
              label="Senha"
              placeholder="Uma senha bem segura"
              type="password"
              register={register}
              name="password"
              error={errors.password?.message}
            />
            <Input
              label="Bio"
              placeholder="Bio"
              register={register}
              name="bio"
              error={errors.bio?.message}
            />
            <Input
              label="Contact"
              placeholder="Contato"
              register={register}
              name="contact"
              error={errors.contact?.message}
            />

            <select
              required
              {...register("course_module")}
              error={errors.course_module?.message}
            >
              <option selected disabled value="">
                Escolha o módulo do curso
              </option>
              <option value="modulo1">
                Primeiro módulo (Introdução ao Frontend)
              </option>
              <option value="modulo2">
                Segundo módulo (Frontend Avançado)
              </option>
              <option value="modulo3">
                Terceiro módulo (Introdução ao Backend)
              </option>
              <option value="modulo4">Quarto módulo (Backend Avançado)</option>
            </select>

            <Button type="submit">Enviar</Button>
            <p>
              Já tem uma conta? Faça seu <Link to="/sessions">login</Link>
            </p>
          </form>
        </AnimationContainer>
      </Content>
    </Container>
  );
};

export default Singup;
