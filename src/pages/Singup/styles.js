import styled, { keyframes } from "styled-components";
import logoKenzieAzul from "../../assets/logoKenzieAzul.png";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  align-items: stretch;
`;

export const Background = styled.div`
  @media (min-width: 1100px) {
    flex: 1;
    background: url(${logoKenzieAzul}) no-repeat center, #04103c;
    background-size: contain;
  }
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  max-width: 700px;
`;

export const appearFromRight = keyframes`
  from {
    opacity: 0;
    transform: translateX(50px)
  }
  
  to {
    opacity:1;
    transform: translateX(0px)
  }
`;

export const AnimationContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  animation: ${appearFromRight} 1s;

  form {
    margin-bottom: 20px;
    width: 340px;
    text-align: center;

    h1 {
      margin-bottom: 18px;
    }

    > div {
      margin-top: 16px;
    }

    select {
      width: 100%;
      margin-top: 20px;
      border-radius: 10px;
      height: 45px;
      flex: 1;
      margin-right: 16px;
      background-color: var(--white);
      color: var(--gray);
      border: 2px solid var(--gray);
      cursor: pointer;
    }

    p {
      margin-top: 8px;

      a {
        font-weight: bold;
        color: var(--blueKenzieHard);
      }
    }
  }
`;
