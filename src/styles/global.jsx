import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  * {
    margin: 0;
    padding:0;
    box-sizing: border-box;
    outline:0;
  }

  :root{
    --white: #f5f5f5;
    --blueKenzie:#2E80FB;
    --black: #0c0d0d;
    --blueKenzieHard: #04103C;
    --gray: #666360;
    --red: #c53030;
  }

  body {
    background-color: var(---blueKenzie);
    color: var(---black)
  }

  body, input, button, select {
    font-family: 'PT Serif', serif;
    font-size: 1rem;
  }

  h1,h2,h3,h4,h5,h6 {
    font-family: 'Roboto Mono', monospace;
    font-weight: 700
  }

  button {
    cursor: pointer
  }

  a{
    text-decoration: none
  }
`;
